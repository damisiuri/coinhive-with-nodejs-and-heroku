var dotenv = require('dotenv')
var http = require('http');
var express = require('express');
var app = express();

var CoinHive = require('coin-hive');
var throttle = parseFloat(process.env.THROTTLE || 0.5);

dotenv.config()

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index');
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

// Start Miners
(function(){
  CoinHive(process.env.COINHIVE).then(function(miner){
    // Setting
    miner.rpc('setThrottle', [throttle]).then(function(){
      miner.start()
    });

    // Events
    miner.on('found', function(){
      console.log('Found!')
    });

    miner.on('accepted', function(){
      console.log('Accepted!')
    });

    miner.on('update', function(data){
      console.log(" \
        \n Hashes per second: " + data.hashesPerSecond + " \
        \n Total hashes: " + data.totalHashes + " \
        \n Accepted hashes: " + data.acceptedHashes + " \
      ")
    });

    // Wakeup servers
    setInterval(function(){
      wakeup_servers(miner)
    }, 5 * 60000) // Each 5 minutes
  });
})();

function wakeup_servers(miner){
  if(process.env.CLUSTERS){
    process.env.CLUSTERS.split(',').forEach(function(host){
      var options = {
        host: host,
        port: 80,
        path: '/'
      };

      http.get(options, function(resp){
        resp.on('data', function(chunk){
          console.log( host + " wakeupped");
        });
      })
    })
  }
}


// Use in Node 8.1.1
// (async () => {
//   // Create miner
//   const miner = await CoinHive(process.env.COINHIVE); // Coin-Hive's Site Key
//
//   // Setting miner
//   await miner.rpc('setThrottle', [0.5]);
//
//
//   // Start miner
//   await miner.start();
//
//   // Stop miner - never stop
//   // setTimeout(async () => await miner.stop(), 60000);
// })();
