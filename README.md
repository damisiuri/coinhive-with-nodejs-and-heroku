# Simple Node Coinhive

## Get Started

```
$ heroku create
```

## Heroku config
Na sua Nova Applica��o Settings>Buildpacks adicione:
```
heroku/nodejs
https://github.com/jontewks/puppeteer-heroku-buildpack
```

### Vari�veis de Ambiente
```
COINHIVE=<SUA CHAVE>
CLUSTERS=<example.com,google.com,yahoo.com.br Opcional>
THROTTLE=<example: 0.2>
```

## Deploy
```
$ git push heroku master
$ heroku open
```